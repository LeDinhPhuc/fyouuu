var express = require("express");
var app = express();

var server = require("http").createServer(app);
var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 6699;
var io = require("socket.io")(server); //80

io.on("connection", function(socket) {
  console.log(socket.id + ": connected");
  socket.emit("id", socket.id);

  socket.on("disconnect", () => {
    console.log(socket.id + ": disconnected");
  });

  socket.on("newMessage", data => {
    io.sockets.emit("newMessage", { data: data, id: socket.id });
    console.log(data);
  });
});

app.get("/", (req, res) => {
  res.send("Home page. Server running okay.");
});

server.listen(port, () => console.log("Server running in port " + port));
