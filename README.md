# PTPMCN

## Usage

#### Run server on port 5000

```shell
cd server
npm run server
```

#### Run client on port 3000

```shell
cd client
npm run client
```
