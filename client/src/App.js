import React from "react";
import Messages from "./components/MessageList";
import Input from "./components/Input";
import _map from "lodash/map";
import io from "socket.io-client";
export default class App extends React.Component {
  constructor(props) {
    super(props);
    //Khởi tạo state,
    this.state = {
      messages: [{ id: 1, userId: "", message: "" }],
      user: null
    };
    this.socket = null;
  }
  //Connetct với server nodejs, thông qua socket.io
  UNSAFE_componentWillMount() {
    this.socket = io("192.168.1.6:6699"); // địa chỉ ip của máy local
    this.socket.on("id", res => this.setState({ user: res })); // lắng nghe event có tên 'id'

    this.socket.on("newMessage", response => {
      this.newMessage(response);
    });
    //lắng nghe event 'newMessage' và gọi hàm newMessage khi có event
  }
  //Khi có tin nhắn mới, sẽ push tin nhắn vào state mesgages, và nó sẽ được render ra màn hình
  newMessage(m) {
    const messages = this.state.messages;
    let ids = _map(messages, "id");
    let max = Math.max(...ids);
    messages.push({
      id: max + 1,
      userId: m.id,
      message: m.data
    });
    this.setState({ messages });
  }
  //Gửi event socket newMessage với dữ liệu là nội dung tin nhắn
  sendNewMessage = m => {
    if (m.value) {
      this.socket.emit("newMessage", m.value); //gửi event về server
      m.value = "";
    }
  };

  render() {
    return (
      <div className="app__content">
        <h1>Demo chat_realtime</h1>
        <div className="chat_window">
          <Messages
            user={this.state.user}
            messages={this.state.messages}
            typing={this.state.typing}
          />
          <Input sendMessage={this.sendNewMessage} />
        </div>
      </div>
    );
  }
}
