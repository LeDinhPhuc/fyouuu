import React from "react";
import "./styles.css";
// import "./item.scss";

export default class MessageItem extends React.Component {
  render() {
    const { message } = this.props;
    console.log("++++++++++++++++++++", message);
    return (
      <li style={{ color: "black" }}>{this.props.userId + ": " + message}</li>
      // <li className={this.props.user ? "message right" : "message left"}>
      //   <div className="avatar">
      //     <img src="" alt="user" />
      //   </div>
      //   <div className="text_wrapper">
      //     <div className="box bg-light-info">{message}</div>
      //   </div>
      //   <div className="time">10:56 am</div>
      // </li>
    );
  }
}
